﻿using System;
using System.Collections.Generic;

namespace CarRental.Core.Models
{
    public partial class Account
    {
        public Account()
        {
            CarOwners = new HashSet<CarOwner>();
            Customers = new HashSet<Customer>();
        }

        public int AccountId { get; set; }
        public string? UserName { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public int? RoleId { get; set; }

        public virtual Role? Role { get; set; }
        public virtual ICollection<CarOwner> CarOwners { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
