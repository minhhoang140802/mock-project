﻿using System;
using System.Collections.Generic;

namespace CarRental.Core.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Bookings = new HashSet<Booking>();
            Feedbacks = new HashSet<Feedback>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; } = null!;
        public DateTime? DateOfBirth { get; set; }
        public string NationalIdNumber { get; set; } = null!;
        public string? PhoneNumber { get; set; }
        public string? Email { get; set; }
        public int? AddressId { get; set; }
        public string DrivingLicense { get; set; } = null!;
        public decimal? Wallet { get; set; }
        public int AccountId { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Address? Address { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
    }
}
