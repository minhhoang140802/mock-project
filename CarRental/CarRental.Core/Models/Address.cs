﻿using System;
using System.Collections.Generic;

namespace CarRental.Core.Models
{
    public partial class Address
    {
        public Address()
        {
            CarOwners = new HashSet<CarOwner>();
            Cars = new HashSet<Car>();
            Customers = new HashSet<Customer>();
        }

        public int AddressId { get; set; }
        public string? City { get; set; }
        public string? District { get; set; }
        public string? Ward { get; set; }
        public string? Detail { get; set; }

        public virtual ICollection<CarOwner> CarOwners { get; set; }
        public virtual ICollection<Car> Cars { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
