﻿using System;
using System.Collections.Generic;

namespace CarRental.Core.Models
{
    public partial class Feedback
    {
        public int FeedbackId { get; set; }
        public decimal? Ratings { get; set; }
        public string? Content { get; set; }
        public DateTime? DateTime { get; set; }
        public int? CustomerId { get; set; }
        public int? BookingId { get; set; }

        public virtual Booking? Booking { get; set; }
        public virtual Customer? Customer { get; set; }
    }
}
