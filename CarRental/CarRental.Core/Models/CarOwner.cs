﻿using System;
using System.Collections.Generic;

namespace CarRental.Core.Models
{
    public partial class CarOwner
    {
        public CarOwner()
        {
            Cars = new HashSet<Car>();
        }

        public int CarOwnerId { get; set; }
        public string CarOwnerName { get; set; } = null!;
        public DateTime? DateOfBirth { get; set; }
        public string NationalIdNumber { get; set; } = null!;
        public string? PhoneNumber { get; set; }
        public string? Email { get; set; }
        public int? AddressId { get; set; }
        public string? DrivingLicense { get; set; }
        public decimal? Wallet { get; set; }
        public int AccountId { get; set; }

        public virtual Account Account { get; set; } = null!;
        public virtual Address? Address { get; set; }
        public virtual ICollection<Car> Cars { get; set; }
    }
}
