﻿using System;
using System.Collections.Generic;

namespace CarRental.Core.Models
{
    public partial class Booking
    {
        public Booking()
        {
            Feedbacks = new HashSet<Feedback>();
        }

        public int BookingId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string? DriverInformation { get; set; }
        public string? PaymentMethod { get; set; }
        public string? Status { get; set; }
        public int CarId { get; set; }
        public int CustomerId { get; set; }

        public virtual Car Car { get; set; } = null!;
        public virtual Customer Customer { get; set; } = null!;
        public virtual ICollection<Feedback> Feedbacks { get; set; }
    }
}
