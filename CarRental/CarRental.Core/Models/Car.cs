﻿using System;
using System.Collections.Generic;

namespace CarRental.Core.Models
{
    public partial class Car
    {
        public Car()
        {
            Bookings = new HashSet<Booking>();
        }

        public int CarId { get; set; }
        public string Name { get; set; } = null!;
        public string LicensePlate { get; set; } = null!;
        public string Brand { get; set; } = null!;
        public string Model { get; set; } = null!;
        public int ColorId { get; set; }
        public int NumberOfSeats { get; set; }
        public string ProductionYears { get; set; } = null!;
        public string TransmissionType { get; set; } = null!;
        public string FuelType { get; set; } = null!;
        public decimal Mileage { get; set; }
        public decimal FuelConsumption { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Deposit { get; set; }
        public int AddressId { get; set; }
        public string? Description { get; set; }
        public string? AdditionalFunctions { get; set; }
        public string? TermsOfUse { get; set; }
        public string? Images { get; set; }
        public int OwnerId { get; set; }

        public virtual Address Address { get; set; } = null!;
        public virtual Color Color { get; set; } = null!;
        public virtual CarOwner Owner { get; set; } = null!;
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
